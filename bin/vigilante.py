#!/usr/local/bin/python
import sys, getopt, json, os, pprint, copy, time, logging, sets
import datetime, dateutil.parser, dateutil.tz
import oauth2client.client, oauth2client.file, oauth2client.tools, apiclient.discovery, httplib2
import boto, boto.ec2, boto.route53.record

def help(message):
    sys.stderr.write("usage: " + sys.argv[0] + "\n")
    sys.stderr.write("  command-line options\n");
    sys.stderr.write("   --help\n")
    sys.stderr.write("   --private_key=            /path/to/google_service_account_private_key.p12\n")
    sys.stderr.write("   --date=                   date-time (with TZ or assumes local timezone) to use for query. Defaults to now\n")
    sys.stderr.write("   --config=                 /path/to/vigilante.json configuration file\n")
    sys.stderr.write("   --aws_access_key_id=      AWS key id (or read from vigilante.json or from boto config)\n")
    sys.stderr.write("   --aws_secret_access_key=  AWS secret key (or read from vigilante.json or from boto config)\n")
    sys.stderr.write("   --dry-run                  Dry run will show the changes that would have been applied\n")
    sys.stderr.write("   --debug         print additional diagnostics\n")
    if message is not None:
        sys.stderr.write("\nError: " + message + "\n")

def parseArguments(argv=None):
    "Allows arguments to be passed in on command-line or read in from a config file. Values in the config file are overridden by command-line arguments"
    if argv==None:
        argv = sys.argv
    defaults = {}
    options = {}
    try:
        opts, args = getopt.getopt(argv[1:],"k:",["date=","private_key=","config=","aws_access_key_id=","aws_secret_access_key=","debug","dry-run","help"])
    except getopt.GetoptError, err:
        help(str(err))
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--help':
            help("Help called!")
            sys.exit()
        elif opt in ('-k', '--private_key'):
            options['private_key'] = arg
        elif opt in ('--date'):
            options['date'] = arg
        elif opt == "--debug":
            options['debug'] = True
        elif opt == "--dry-run":
            options['dry-run'] = True
	elif opt == '--config':
	    defaults = load_json(arg)
        elif opt == '--aws_secret_access_key':
            options['aws_secret_access_key'] = arg
        elif opt == '--aws_access_key_id':
            options['aws_access_key_id'] = arg
    # Attempt to read config file as the first argument instead (if not already loaded)
    if defaults=={} and len(args)>0:
        defaults = load_json(args[0])
    options = dict(defaults.items() + options.items())
    if not 'debug' in options:
	options['debug'] = False
    if not 'dry-run' in options:
        options['dry-run'] = False
    if 'private_key' not in options:
        help("must specify the path to private_key")
        sys.exit(3)
    if options['debug']:
        consoleHandler.setLevel(logging.INFO)
        logging.info("Options: %s", options)
    return options

def load_json(filename):
    "Loads a json document from a file"
    f = open(filename,'r')
    config = json.load(f)
    f.close()
    return config

def load_text(filename):
    "Returns the contents of a file as a string"
    file = open(filename, 'r')
    content = file.read()
    file.close()
    return content
    
def getCalendarService(args):
    "Creates a proxy instance to the Google Calendar API using service account credentials"
    credentials = oauth2client.client.SignedJwtAssertionCredentials(service_account_name=args['service_account_email'], private_key=load_text(args['private_key']), scope='https://www.googleapis.com/auth/calendar.readonly')
    http = credentials.authorize(httplib2.Http())
    return apiclient.discovery.build(serviceName='calendar', version='v3', http=http, developerKey=args['developer_key'])

def calculateQueryTimeRange(dateToParse):
    "Parses a date, returning a tuple with the date and date+1hour. If the dateToParse is None or blank, uses the current system time. If no timezone specified, assumes the machine's local timezone."
    if dateToParse!=None and dateToParse!="":
        startTime = dateutil.parser.parse(dateToParse)
        # If no timezone was specified, assume local timezone on the machine
        if startTime.tzinfo==None:
            startTime = startTime.replace(tzinfo=dateutil.tz.tzlocal())
    else:
        startTime = datetime.datetime.now(dateutil.tz.tzlocal())
    return (startTime, startTime + datetime.timedelta(seconds=1))

def formatTimeAsISO(timeToFormat):
    "Formats a time object into ISO-8601 format"
    return timeToFormat.strftime("%Y-%m-%dT%H:%M:%S%z")

def parseAndAddFilters(filters,contentToParse):
    "Parses a multi-line string for EC2 filters. Ignores comments and blank lines. Other lines are key-value pairs for EC2 API filters"
    # Keep track of whether we actually found any filters or not, if no filters specified, then ignore the event (so we don't start everything()
    anyFiltersSpecified = False
    for line in contentToParse.split("\n"):
        if line!='' and line[0]!='#':
            anyFiltersSpecified = True
            if line!="*" and line!="all":
               parts = line.split("=",2)
               # TODO - Need to handle bad rows missing ='s gracefully so one bad calendar doesn't mess us up
               if len(parts)==2:
                   if parts[0] in filters:
                       if not isinstance(value, list):
                           if value!=filters[parts[0]]:
                               parts[0] = [ parts[0], value ]
                       else:
                           if value not in filters[parts[0]]:
                               parts[0].append(value)
                   else:
                       filters[parts[0]] = parts[1]
    return anyFiltersSpecified

def getScopeFiltersFromCalendar(calendar):
    "Creates the set of scope filters for a calendar. This represents the servers that are considered to be managed. Non matching servers are ignored by vigilante."
    filters = {}
    parseAndAddFilters(filters, calendar['description'])
    return filters;

def getCurrentEventFiltersFromCalendar(calendar, timerange, scope, calendar_service):
    "Enumerates events during the specified time range on the calendar, extracting filters for which servers should be running"
    filterset = []
    events_response = calendar_service.events().list(calendarId=calendar['id'], timeMin=formatTimeAsISO(timerange[0]), timeMax=formatTimeAsISO(timerange[1])).execute()
    if "items" in events_response:
        for event in filter(lambda e : 'description' in e, events_response['items']):
            logging.info("Found event description = " + event['description'])
            # Need a deep copy of scope so we can add to it without affecting the original
            event_filters = copy.deepcopy(scope) 
            if parseAndAddFilters(event_filters, event['description']):
                # If we didn't find any filters, then ignore this event, otherwise it defaults to "all items", which might not be what the user intended.
                filterset.append(event_filters) 
    return filterset
        
def printDryRunDNSRegistrations(instancesByRegion):
    "Prints out the DNS registrations that would have been executed in a real run"
    for region in instancesByRegion:
        for instance in instancesByRegion[region]:
            logging.warn("DRYRUN - Would have setup DNS CNAME for %s:%s as %s.%s", region.name, instance.id, instance.tags['route53.cname'], instance.tags['route53.zone'])

def pruneNonDNSInstancesByRegion(instancesByRegion):
    "Removes server instances that do not have DNS tags from consideration"
    for region in instancesByRegion:
        instancesToBeRemoved = []
        for instance in instancesByRegion[region]:
            if "route53.zone" not in instance.tags:
                logging.info("Disregarding instance %s because it isn't tagged with route53.zone", instance)
                instancesToBeRemoved.append(instance)
        for toRemove in instancesToBeRemoved:
            instancesByRegion[region].remove(toRemove)

def pruneEmptyRegions(instancesByRegion):
    "Removes regions that do not have any DNS tagged servers"
    regionsToBeRemoved = [] 
    for region in instancesByRegion:
        if len(instancesByRegion[region])==0:
            logging.info("Disregarding region %s because it doesn't have any instances tagged with route53.zone", region)
            regionsToBeRemoved.append(region)
    for toRemove in regionsToBeRemoved:
        del instancesByRegion[toRemove]

def setRoute53(instance, zoneCache, route53):
    zoneName = instance.tags['route53.zone']
    cname = instance.tags['route53.cname'] + "." + zoneName
    if zoneName not in zoneCache:
        zoneId = route53.get_hosted_zone_by_name(zoneName)['GetHostedZoneResponse']['HostedZone']['Id'].replace('/hostedzone/','')
        zoneCache[zoneName] = route53.get_all_rrsets(zoneId)
    zone = zoneCache[zoneName]
    for record in zone:
        logging.info("Found existing record %s", record.name)
        if record.name==cname+".":
            logging.info("Deleting cname record for %s", cname)
            change = zone.add_change("DELETE", cname, "CNAME", record.ttl)
            if type(record.resource_records) in [list, tuple, set]:
                for value in record.resource_records:
                    change.add_value(value)
            else:
                change.add_value(record.resource_records)
            break
    logging.warn("Creating cname record for %s as %s", cname, instance.public_dns_name)
    zone.add_change("CREATE", cname, "CNAME", 60).add_value(instance.public_dns_name)

def prepareDNSChanges(instancesByRegion, route53, zoneCache, waitCyclesRemaining):
    logging.info("Attempting to process DNS registrations for all regions %s cycles remaining", waitCyclesRemaining)
    logging.info("Started Instance By Region = %s", instancesByRegion)
    anyRemaining = False
    for region in instancesByRegion:
        registeredInstances = []
        for instance in instancesByRegion[region]:
            updated_instance = region.connection.get_all_instances([instance.id])[0].instances[0]
            if updated_instance.public_dns_name!='':
                logging.info("Found public DNS name %s for instance %s", updated_instance.public_dns_name, instance)
                setRoute53(updated_instance, zoneCache, route53)
                registeredInstances.append(instance)
            else:
                logging.info("Public DNS name for server %s not yet available.", instance)
                anyRemaining = True
        for instance in registeredInstances:
            instancesByRegion[region].remove(instance)
    if anyRemaining:
        pruneEmptyRegions(instancesByRegion)
        if waitCyclesRemaining>0:
            logging.info("There are still unresolved DNS updates. Will wait 30 seconds for machines to startup and try again.")
            time.sleep(30)
            prepareDNSChanges(instancesByRegion, route53, zoneCache, waitCyclesRemaining-1)                        
        else:
            logging.error("DNS value for new instance did not become available in time to set DNS. Remaining Instances %s", instancesByRegion)

def processUpdatesInRegion(connection, dryrun, scope, filter_set):
    "For a single region: reconciles the list of servers that are in scope for management vs. ones that should be running, taking start and stop action on instances if required."
    startedInstances = []
    shouldBeRunning = sets.Set();
    for filter in filter_set:
        for reservation in connection.get_all_instances(filters = filter):
            for instance in reservation.instances:
                shouldBeRunning.add(instance.id)
                logging.info("Instance should be running - " + instance.id + " which is currently " + instance.state)
    for reservation in connection.get_all_instances(filters = scope):
        for instance in reservation.instances:
            logging.info("Found instance - %s which is current %s, and should be running=%s", instance.id,  instance.state, instance.id in shouldBeRunning)
            if(instance.id in shouldBeRunning):
                # startedInstances.append(instance) # PJM - force DNS updates to all running servers
                if instance.state=="stopped":
                    startedInstances.append(instance)
                    if dryrun:
                        logging.warn("DRYRUN - would have started %s:%s", connection.region.name, instance.id)
                    else:
                        logging.warn("Starting - " + instance.id)
                        # NOTE : Calling instance.stop() replaces some of the valuable metadata in the object (such as the dns values), so call below in batch instead
            else:
                if instance.state=="running":
                    if dryrun:
                        logging.warn("DRYRUN - would have stopped %s:%s", connection.region.name, instance.id)
                    else:
                        logging.warn("Stopping - " + instance.id)
                        instance.stop()
    if not dryrun and len(startedInstances)>0: 
        connection.start_instances(map(lambda i : i.id, startedInstances))
    return startedInstances

def processUpdatesInAllRegions(args, scope, filter_set):
    "For all regions: reconciles the list of servers that are in scope for management vs. ones that should be running, taking start and stop action on instances if required."
    logging.info("Processing all ec2-regions")
    instancesByRegion = {}
    for region in boto.ec2.regions(aws_access_key_id=args.get('aws_access_key_id'), aws_secret_access_key=args.get('aws_secret_access_key')):
        logging.info("Processing region - " + str(region))
        instancesByRegion[region] = processUpdatesInRegion(region.connect(aws_access_key_id=args.get('aws_access_key_id'), aws_secret_access_key=args.get('aws_secret_access_key')), args['dry-run'], scope, filter_set)
    pruneNonDNSInstancesByRegion(instancesByRegion)
    pruneEmptyRegions(instancesByRegion)
    if args['dry-run']:
        printDryRunDNSRegistrations(instancesByRegion)
    else:
        zoneCache = {}
        prepareDNSChanges(instancesByRegion, boto.connect_route53(aws_access_key_id=args.get('aws_access_key_id'), aws_secret_access_key=args.get('aws_secret_access_key')), zoneCache, 4)
        for zone in zoneCache.values():
            zone.commit()
        
def execute(args):
    calendar_service = getCalendarService(args)
    timerange = calculateQueryTimeRange(args.get("date"))
    # Start by finding only calendars that have #!vigilante at the beginning of their description
    for calendar in filter(lambda c : "description" in c and c["description"].startswith("#!vigilante"), calendar_service.calendarList().list().execute()['items']):
        # The scope filters indicate that only servers matching the specified filters are considered "managed"
        scope = getScopeFiltersFromCalendar(calendar)
        logging.info("Scope filters for calendar %s are: %s", calendar['summary'], scope)
        filter_set = getCurrentEventFiltersFromCalendar(calendar, timerange, scope, calendar_service)
        logging.info("Scheduled filters for calendar %s are: %s", calendar['summary'], filter_set)
        processUpdatesInAllRegions(args, scope, filter_set)

def main(argv):
    execute(parseArguments())

consoleHandler = logging.StreamHandler(sys.stderr)
if __name__ == "__main__":
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    logging.basicConfig(filename='vigilante.log', filemode='a', level=logging.DEBUG, format='%(asctime)s|%(levelname)s|%(name)s|%(message)s')
    consoleHandler.setLevel(logging.WARN)
    logging.root.addHandler(consoleHandler)
    main(sys.argv)
